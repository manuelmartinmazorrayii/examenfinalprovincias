<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Provincias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provincias-index">


    <div class="jumbotron">
        <h1>Consultas con DAO y ORM</h1>
    </div>

    <div class="body-content">
        <div class="row">
            <!--
            -->
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <h3>consulta 1</h3>
                    <P>La provincia mas despoblada de la autonomia menos extensa teniendo en cuenta solo las autonomias"
            . "de más de 2 provincias (con DAO sin utilizar la clausula LIMIT)</P>
                    <p>
                        <?= Html::a('DAO', ['site/consultadao'], ['class' => 'btn btn-primary'])?> 
                    </p>
                </div>
            </div>
        </div>
    </div>


</div>
