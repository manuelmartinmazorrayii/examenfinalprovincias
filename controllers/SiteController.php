<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\models\Provincias;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsultadao()
    {
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT provincia FROM provincias 
                WHERE poblacion = (
                  SELECT MIN(c3.poblacion) as popmin FROM
                    (
                    SELECT poblacion, provincia, autonomia FROM provincias 
                      WHERE autonomia =  
                      (
                          SELECT c1.autonomia FROM 
                        (
                        SELECT autonomia, superficie FROM provincias 
                          GROUP BY autonomia HAVING COUNT(provincia) > 2
                        )c1

                        WHERE c1.superficie = 
                        (
                          SELECT MAX(c2.superficie) FROM 
                            (
                              SELECT autonomia, superficie FROM provincias 
                              GROUP BY autonomia HAVING COUNT(provincia) > 2
                            )c2
                        )
                      )
                    )c3
                )',
            'totalCount'=>5,
            'pagination'=>['pageSize' => 5,]
        ]);
        
        return $this->render("resultado", [
           "resultado"=>$dataProvider,
            "campos"=>['provincia'],
            "titulo"=>"Consulta provincias con DAO",
            "enunciado"=>"La provincia mas despoblada de la autonomia menos extensa teniendo en cuenta solo las autonomias"
            . "de más de 2 provincias (con DAO sin utilizar la clausula LIMIT)",
            "sql"=>"Para llegar a esta consulta he hecho los siguiente:"
            . "1: agrupe las autonomias por su superficie, teniendo en cuenta que solo aparezcan las que tienen mas de 2 provincias."
            . "2: a base de estos resultados, busce la superficie mas alta de todas."
            . "3: con la superficie mas alta, busce cual de las autonomias tiene esa superficia"
            . "4: a partir de aqui buscamos datos de autonomias usando los datos anteriores. Buscamos la poblacion de cada provincia de cada autonomia"
            . "5: despues, con estos resultados, recogemos la poblacion mas pequeña"
            . "6: finalmente, simplemente miramos con un WHERE cual provincia tiene esa poblacion.",
        ]);
    }
}